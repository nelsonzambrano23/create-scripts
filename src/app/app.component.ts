import { Component, inject } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { RouterOutlet } from '@angular/router';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, ReactiveFormsModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {
  datosJSON: any;
  datosSQL: any;
  formGroup!: FormGroup;
  fb = inject(FormBuilder);

  constructor() {
    this.initForm();
  }

  initForm() {
    this.formGroup = this.fb.group({
      collection: [''],
      tabla: [''],
      file: [''],
    });
  }

  onFileChange(event: any) {
    this.datosJSON = null;
    const target: DataTransfer = <DataTransfer>event.target;
    if (target.files.length !== 1)
      throw new Error('No se puede usar múltiples archivos');

    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* Leer el archivo de Excel */
      const binaryStr: string = e.target.result;
      const libro: XLSX.WorkBook = XLSX.read(binaryStr, { type: 'binary' });

      /* Asumir que estamos trabajando con la primera hoja */
      const nombreHoja: string = libro.SheetNames[0];
      const hoja: XLSX.WorkSheet = libro.Sheets[nombreHoja];

      /* Convertir la hoja a JSON */
      const datos: any[] = XLSX.utils.sheet_to_json(hoja);

      /* Filtrar las columnas deseadas */
      const columnasDeseadas = [
        'identification',
        'firstName',
        'secondName',
        'firstLastname',
        'secondLastName',
        'typePersonId',
        'documentType',
      ];

      let datosFiltradosJSON: String[] = [];
      let datosFiltradosSQL: String[] = [];

      // JSON para MongoDB
      datos.forEach((fila) => {
        const nuevaFila: any = {};
        columnasDeseadas.forEach((columna) => {
          if (fila[columna] !== undefined) {
            if (columna !== 'identification') {
              nuevaFila[columna] = fila[columna];
            }
          }
        });
        datosFiltradosJSON.push(
          `db.${
            this.formGroup.get('collection')?.value
          }.updateOne({ identification: "${
            fila['identification']
          }" }, { $set: ${JSON.stringify(nuevaFila)} })`
        );

        // SQL para MySQL

        datosFiltradosSQL.push(
          `UPDATE sesion SET type_person = 1 WHERE login = ${fila['identification']};`
        );
      });
      /* Guardar los datos filtrados en JSON */
      this.datosJSON = datosFiltradosJSON.join(', \n');

      /* Guardar los datos filtrados en SQL */
      this.datosSQL = datosFiltradosSQL.join('\n');
    };
    reader.readAsBinaryString(target.files[0]);
    this.formGroup.get('file')?.setValue('');
  }
  copyJson() {
    navigator.clipboard.writeText(this.datosJSON);
  }
  copySql() {
    navigator.clipboard.writeText(this.datosSQL);
  }
}
